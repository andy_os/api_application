#my_comment3
from flask import Flask, jsonify
from flaskext.mysql import MySQL
import pymysql
import logging

logging.basicConfig(level = logging.DEBUG, filename = "my.log")
app = Flask(__name__)
mysql = MySQL()

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'Syngenta.1234'
app.config['MYSQL_DATABASE_DB'] = 'USERS_DATABASE'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config["DEBUG"] = True
mysql.init_app(app)


@app.route('/api/v1/resources/users/<id>', methods=['GET'])
def users_id(id):
  conn = mysql.connect()
  cursor = conn.cursor()
  cursor.execute("SELECT * FROM USERS_DATABASE.USERS where id = (%s)", id)
  row = cursor.fetchone()
  resp = jsonify(row)
  return resp
  cursor.close() 
  conn.close()
  logging.debug("Information was requested")  


@app.route('/', methods=['GET'])
def get():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("select * from USERS_DATABASE.USERS")
    row = cursor.fetchall()
    resp = jsonify(row)
    return resp
    cursor.close() 
    conn.close()

if __name__ == '__main__':
    app.run(host = "0.0.0.0",port = "5000")
